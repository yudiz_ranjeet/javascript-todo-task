const signUp = e => {
    let name = document.getElementById('name').value,
        email = document.getElementById('email').value,
        mno = document.getElementById('m-no').value,
        gender = document.getElementsByName('gender').value,
        dob = document.getElementById('dob').value,
        psw = document.getElementById('psw').value;

    let formData = JSON.parse(localStorage.getItem('formData')) || [];

    let exist = formData.length &&
        JSON.parse(localStorage.getItem('formData')).some(data =>
            data.name.toLowerCase() == name.toLowerCase() && data.email == email);

    if (!exist) {
        formData.push({ name, email, mno, gender, dob, psw });
        localStorage.setItem('formData', JSON.stringify(formData));
        document.querySelector('form').reset();
        document.getElementById('name').focus();
        document.getElementById('email').focus();
        alert("Account Created please sign-in");
        window.location.href = "/signin.html";
    } else {
        alert("ooppss...Duplicate you have already sign-up");
    }
    e.preventDefault();
}

const signIn = e => {
    let inputEmail = document.getElementById('email').value,
        psdd = document.getElementById('psw').value;
    let formData = JSON.parse(localStorage.getItem('formData')) || [];
    if (formData.length > 0) {
        let exit = JSON.parse(localStorage.getItem('formData')).find(data => data.email == inputEmail && data.psw == psdd);
        if (!exit) {
            alert("!Incorrect email id or password");
        } else {
            e.preventDefault();
            window.location.href = "/work.html?email=" + exit.email;
        }
    } else {
        alert("No any registration yet. Please Signup")
    }
}

function getUserDetails() {
    var button = document.querySelector('.button3');
    var name = document.querySelector(".nam");
    var mail = document.querySelector(".mail");
    var bday = document.querySelector(".bday");
    var mobile = document.querySelector(".mobile");
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    let userData = JSON.parse(localStorage.getItem('formData')).find(data => data.email == params.email);
    button.addEventListener('click', function() {
        name.innerHTML = userData.name;
        bday.innerHTML = userData.dob;
        mail.innerHTML = userData.email;
        mobile.innerHTML = userData.mno;
    })
}

function logout() {
    var button = document.querySelector('#button2');
    button.addEventListener('click', function() {
        window.location.href = "/home.html"
    })
}


function weather() {
    var button = document.querySelector('.button1');
    var inputValue = document.querySelector('.inputValue');
    var name1 = document.querySelector('.name2');
    var temp = document.querySelector('.temp');
    var desc = document.querySelector('.desc');

    button.addEventListener('click', function() {
        fetch('https://api.openweathermap.org/data/2.5/weather?q= ' + inputValue.value + '&appid=eae9c14c78add82ffe5326b7d7889e20')
            .then(response => response.json())
            .then(data => {
                var nameValue = data['name'];
                var tempValue = data['main']['temp'];
                var descValue = data['weather'][0]['description'];
                name1.innerHTML = nameValue;
                temp.innerHTML = Math.floor(tempValue - 273.15) + " degree celsius";
                desc.innerHTML = descValue;
            })
            .catch(err => alert("wrong city name!"))
        var tt = setTimeout(weather, 20000)
    })
}

function realtimeClock() {
    var rtClock = new Date();
    var hours = rtClock.getHours();
    var minutes = rtClock.getMinutes();
    var seconds = rtClock.getSeconds();
    var amPm = (hours < 12) ? "am" : "pm";
    hours = (hours > 12) ? hours - 12 : hours;
    hours = ("0" + hours).slice(-2);
    minutes = ("0" + minutes).slice(-2);
    seconds = ("0" + seconds).slice(-2);
    document.getElementById("clock").innerHTML = hours + ":" + minutes + ":" + seconds + " " + amPm;
    var tt = setTimeout(realtimeClock, 500);
}

function submit() {
    var sub = document.getElementById("button2");
    sub.addEventListener('click', function() {
        window.location.href = "/home.html";
    });
}

function gif() {
    function take_gif() {
        var apikey = "LIVDSRZULELA";
        var lmt = 200;
        var search_term = ["swag", "happy", "sad", "dance", "funny", "laugh", "sorry", "work", "animal", "cry"];
        var search_url = "https://g.tenor.com/v1/random?q=" + search_term[Math.floor((Math.random() * 10) + 1)] + "&key=" +
            apikey + "&limit=" + lmt;
        httpGetAsync(search_url, random_search);
        var tt = setTimeout(take_gif, 120000)
        return;
    }

    function httpGetAsync(theUrl, callback) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                callback(xmlHttp.responseText);
            }
        }
        xmlHttp.open("GET", theUrl, true);
        xmlHttp.send(null);
        return;
    }

    function random_search(responsetext) {
        var response_objects = JSON.parse(responsetext);
        top_10_gifs = response_objects["results"];
        document.getElementById("preview_gif").src = top_10_gifs[0]["media"][0]["nanogif"]["url"];
        return;
    }
    take_gif();

}


function todo() {

    var valueTask = document.getElementById("new-task");
    var addButton = document.getElementsByTagName("button")[0];
    var remain = document.getElementById("incomplete-tasks");
    var done = document.getElementById("completed-tasks");

    var newTask = function(taskString) {
        var listItem = document.createElement("li");
        var checkBox = document.createElement("input");
        var label = document.createElement("label");
        var editInput = document.createElement("input");
        var editButton = document.createElement("button");
        var deleteButton = document.createElement("button");
        label.innerText = taskString;

        checkBox.type = "checkbox";
        editInput.type = "text";

        editButton.innerText = "Edit";
        editButton.className = "edit";
        deleteButton.innerText = "del";
        deleteButton.className = "delete";

        listItem.appendChild(checkBox);
        listItem.appendChild(label);
        listItem.appendChild(editInput);
        listItem.appendChild(editButton);
        listItem.appendChild(deleteButton);
        return listItem;
    }



    var addTask = function() {
        console.log("Add Task...");
        var listItem = newTask(valueTask.value);
        remain.appendChild(listItem);
        bindTaskEvents(listItem, taskCompleted);
        valueTask.value = "";

    }

    var editTask = function() {
        console.log("Edit Task...");
        console.log("Change 'edit' to 'save'");
        var listItem = this.parentNode;
        var editInput = listItem.querySelector('input[type=text]');
        var label = listItem.querySelector("label");
        var containsClass = listItem.classList.contains("editMode");
        if (containsClass) {
            label.innerText = editInput.value;
        } else {
            editInput.value = label.innerText;
        }

        listItem.classList.toggle("editMode");
    }

    var deleteTask = function() {
        console.log("Delete Task...");
        var listItem = this.parentNode;
        var ul = listItem.parentNode;
        ul.removeChild(listItem);
    }

    var taskCompleted = function() {
        console.log("Complete Task...");
        var listItem = this.parentNode;
        done.appendChild(listItem);
        bindTaskEvents(listItem, taskIncomplete);
    }

    var taskIncomplete = function() {
        console.log("Incomplete Task...");
        var listItem = this.parentNode;
        remain.appendChild(listItem);
        bindTaskEvents(listItem, taskCompleted);
    }

    addButton.onclick = addTask;
    addButton.addEventListener("click", addTask);

    var bindTaskEvents = function(taskListItem, checkBoxEventHandler) {
        console.log("bind list item events");
        var checkBox = taskListItem.querySelector("input[type=checkbox]");
        var editButton = taskListItem.querySelector("button.edit");
        var deleteButton = taskListItem.querySelector("button.delete");

        editButton.onclick = editTask;
        deleteButton.onclick = deleteTask;
        checkBox.onchange = checkBoxEventHandler;
    }

    for (var i = 0; i < remain.children.length; i++) {
        bindTaskEvents(remain.children[i], taskCompleted);
    }

    for (var i = 0; i < done.children.length; i++) {
        bindTaskEvents(done.children[i], taskIncomplete);
    }
}